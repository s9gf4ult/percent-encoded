module Text.PercentEncoded
       ( PctEncodable(..)
         -- * Alphabets for 'fromRaw'
       , upperAlpha
       , lowerAlpha
         -- * Prefixes for 'fromRaw'
       , urlEnc
         -- * Creating 'PctEncoded'
       , fromEncoded
       , fromRaw
       , urlFromRaw
         -- * Extracting raw/encoded data
       , PctEncoded
       , getRaw
       , getEncoded
       ) where

import Text.PercentEncoded.Internal



urlFromRaw :: (PctEncodable a) => a -> PctEncoded a
urlFromRaw = fromRaw upperAlpha urlEnc
