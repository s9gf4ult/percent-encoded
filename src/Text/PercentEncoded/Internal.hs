module Text.PercentEncoded.Internal
       ( PctEncodable(..)
       , PctEncoded(..)
       , lowerAlpha
       , upperAlpha
       , fromRaw
       , fromEncoded
       , urlEnc
       ) where

import Control.DeepSeq
import Data.ByteString.Internal ( w2c, c2w )
import Data.CaseInsensitive ( FoldCase(..) )
import Data.Char ( toLower )
import Data.Maybe ( isJust, fromJust )
import Data.Monoid
import Data.String ( IsString(..) )
import Data.Text.Encoding.Error ( lenientDecode )
import Data.Typeable ( Typeable )
import Data.Word
import GHC.Generics ( Generic )
import Numeric ( showIntAtBase, readInt )

import qualified Data.ByteString as B
import qualified Data.ByteString.Builder as BB
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TL


upperAlpha :: B.ByteString
upperAlpha = "0123456789ABCDEF"

lowerAlpha :: B.ByteString
lowerAlpha = "0123456789abcdef"


-- | Byte sequence which can be percent encoded and decoded
class PctEncodable a where
    -- | Encode string
    pctEncode :: B.ByteString    -- ^ 16 symbols alphabet
              -> (Word8 -> Bool) -- ^ If true this byte will be percent encoded (but __%__ symbol should always be encoded)
              -> a              -- ^ Raw data to encode
              -> a
    pctDecode :: a               -- ^ Encoded string to decode
              -> a


pencodeBB :: B.ByteString -> (Word8 -> Bool) -> [Word8] -> BL.ByteString
pencodeBB alpha f w = BB.toLazyByteString
                      $ mconcat
                      $ map toB w
  where
    toB b = if (w2c b == '%') || (f b)
            then BB.char7 '%' <> BB.string7 (encstr b)
            else BB.word8 b
    encstr b =
        let res = showIntAtBase 16 (BC.index alpha) b []
        in if (length res) < 2
           then (BC.index alpha 0):res
           else res

pdecodeBB :: [Word8] -> BB.Builder
pdecodeBB (a:b:c:xs) | w2c a == '%' = BB.word8 decb <> pdecodeBB xs
                     | otherwise = BB.word8 a <> pdecodeBB (b:c:xs)
  where
    decb =
        let s = [w2c b, w2c c]
            fromCh ch = BC.elemIndex (toLower ch) lowerAlpha
            chPred = isJust . fromCh
            chInt = fromJust . fromCh
        in case readInt 16 chPred chInt s of
               [(res, "")] -> res
               _ -> error "Wrong char in percent encoding occured"

pdecodeBB (x:xs) = BB.word8 x <> pdecodeBB xs
pdecodeBB [] = mempty


instance PctEncodable B.ByteString where
    pctEncode alpha f bs = BL.toStrict
                           $ pencodeBB alpha f
                           $ B.unpack bs
    pctDecode bs = BL.toStrict
                   $ BB.toLazyByteString
                   $ pdecodeBB
                   $ B.unpack bs

instance PctEncodable BL.ByteString where
    pctEncode alpha f bs = pencodeBB alpha f
                           $ BL.unpack bs

    pctDecode bs = BB.toLazyByteString
                   $ pdecodeBB
                   $ BL.unpack bs

-- | Using UTF-8 encoding to represent text as sequence of octets
instance PctEncodable T.Text where
    pctEncode alpha f t = T.decodeUtf8
                          $ pctEncode alpha f
                          $ T.encodeUtf8 t
    pctDecode = T.decodeUtf8With lenientDecode . pctDecode . T.encodeUtf8

-- | Using UTF-8 encoding to represent text as sequence of octets
instance PctEncodable TL.Text where
    pctEncode alpha f t = TL.decodeUtf8
                          $ pctEncode alpha f
                          $ TL.encodeUtf8 t
    pctDecode = TL.decodeUtf8With lenientDecode . pctDecode . TL.encodeUtf8

-- | Using UTF-8 encoding to represent text as sequence of octets
instance PctEncodable String where
    pctEncode alpha f s = TL.unpack
                          $ TL.decodeUtf8
                          $ pctEncode alpha f
                          $ TL.encodeUtf8
                          $ TL.pack s
    pctDecode = TL.unpack
              . TL.decodeUtf8
              . pctDecode
              . TL.encodeUtf8
              . TL.pack

-- | Return true if symbol must percent-encoded in URL component
urlEnc :: Word8 -> Bool
urlEnc w = not $ (w >= c2w 'a' && w <= c2w 'z')
               || (w >= c2w 'A' && w <= c2w 'Z')
               || (w >= c2w '0' && w <= c2w '9')
               || (B.elem w "-._~")


-- | Percent encoded text
data PctEncoded b =
    PctEncoded
    { getRaw         :: b            -- ^ Get raw (decoded) text
    , getEncoded     :: b            -- ^ Get percent-encoded text
    , getEncAlpha    :: B.ByteString -- ^ Get alphabet for hex encoding
    , getEncFunction :: Word8 -> Bool -- ^ Get byte encoder function
    } deriving (Typeable, Generic)

fromEncoded :: (PctEncodable a)
            => a                -- ^ Encoded string
            -> PctEncoded a
fromEncoded a = PctEncoded
    { getRaw         = pctDecode a
    , getEncoded     = a
    , getEncAlpha    = upperAlpha
    , getEncFunction = urlEnc
    }

fromRaw :: (PctEncodable a)
        => B.ByteString         -- ^ Alphabet to encode hex numbers
        -> (Word8 -> Bool)      -- ^ If return true then byte will be percent-encoded
        -> a                    -- ^ Raw string
        -> PctEncoded a
fromRaw alpha f a = PctEncoded
    { getRaw         = a
    , getEncoded     = pctEncode alpha f a
    , getEncAlpha    = alpha
    , getEncFunction = f
    }

mapPctEncoded :: (PctEncodable s) => (s -> s) -> PctEncoded s -> PctEncoded s
mapPctEncoded f penc =
    let raw = getRaw penc
        res = f raw
        alpha = getEncAlpha penc
        efun = getEncFunction penc
    in PctEncoded
       { getRaw         = res
       , getEncoded     = pctEncode alpha efun res
       , getEncAlpha    = alpha
       , getEncFunction = efun
       }

instance (Eq b) => Eq (PctEncoded b) where
    a == b = (getRaw a) == (getRaw b)

instance (Ord b) => Ord (PctEncoded b) where
    compare a b = compare (getRaw a) (getRaw b)

instance (Show b) => Show (PctEncoded b) where
    show a = show $ getEncoded a

instance (PctEncodable s, IsString s) => IsString (PctEncoded s) where
    fromString s = fromEncoded $ fromString s

instance (PctEncodable s, FoldCase s) => FoldCase (PctEncoded s) where
    foldCase penc = mapPctEncoded foldCase penc

instance (NFData b) => NFData (PctEncoded b)
