module Main where

import Data.Monoid
import Test.QuickCheck.Assertions
import Test.QuickCheck.Instances ()
import Test.Tasty
import Test.Tasty.QuickCheck
import Text.PercentEncoded

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL

prop_enc :: (PctEncodable s, Show s, Eq s) => s -> Property
prop_enc t =
    (t ==? (pctDecode $ pctEncode lowerAlpha urlEnc t))
    .&&. (t ==? (pctDecode $ pctEncode upperAlpha (const False) t))
    .&&. (t ==? (pctDecode $ pctEncode upperAlpha (const True) t))


main :: IO ()
main = do
    defaultMain $ testGroup "properties"
        [ testGroup "decode . encode ~ id"
          [ testProperty "ByteString" (prop_enc :: B.ByteString -> Property)
          , testProperty "lazy ByteString" (prop_enc :: BL.ByteString -> Property)
          , testProperty "Text" (prop_enc :: T.Text -> Property)
          , testProperty "lazy Text" (prop_enc :: TL.Text -> Property)
          , testProperty "String" (prop_enc :: String -> Property)
          ]
        ]
